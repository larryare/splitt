export default function Header() {
  return (
    <header className="border border-black p-2">
      <h1 className="text-xl">Splitt</h1>
    </header>
  );
}
