export default function InfoBar() {
  return (
    <div className="bg-gray-400 my-4 p-2 rounded-xl text-sm">
      No expenses yet, add the first one.
    </div>
  );
}
