import Link from "next/link";

function LinkButton(props: { children: any; href: string; active: boolean }) {
  const active = props.active ? "border border-green-500" : "";
  return (
    <Link className={"rounded-sm p-4 " + active} href={props.href}>
      {props.children}
    </Link>
  );
}

export default function Tabbar({ id }: { id: string }) {
  return (
    <div className="flex flex-row text-sm text-gray-500">
      <LinkButton active={true} href={`/groups/${id}`}>
        Expenses
      </LinkButton>
      <LinkButton href={`/groups/${id}/balances`}>Balances</LinkButton>
      <LinkButton href={`groups/${id}/edit`}>Edit Group</LinkButton>
    </div>
  );
}
