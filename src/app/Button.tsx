export default function Button({ light, ...props }) {
  const type = props.light
    ? "bg-green-300 hover:bg-green-400 text-green-800"
    : "bg-green-800 hover:bg-green-600 text-white";
  return (
    <button className={"shadow-lg rounded-lg py-2 px-4 " + type}>
      {props.children}
    </button>
  );
}
